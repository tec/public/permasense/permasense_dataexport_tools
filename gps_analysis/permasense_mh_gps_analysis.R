## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
##
## PermaSense Matterhorn GPS Data Analysis Tool
##
## This allows to analyze PermaSense Matterhorn kinematic data
## - Plotting
## - Data Export
##
## @author: Jan Beutel
## @date:   October 20, 2017
## @version v1.2
##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##


##====================================================================
## PREPARATION
require(xts)

#setwd("~/permasense_engineering/software/gps/gps_permos_toolchain")
setwd("V:/scratch/permasense_dataexport_tools/gps_analysis")

## construct method to concatenate strings with ampersand
"&" <- function(...) UseMethod("&") 
"&.default" <- .Primitive("&") 
"&.character" <- function(...) paste(...,sep="")

positions <- list(42, 33, 34, 35, 40, 43)
#date_beg <- "03/02/2011+00:00:00"
date_beg <- "15/02/2011+00:00:00"
#date_beg <- "01/01/2018+00:00:00"
date_end <- strftime(Sys.Date(), format="%d/%m/%Y+%H:%M:%S")

for (position in positions) {
  #position = 42
  virtual_sensor <- "matterhorn_gps_differential__rtklib__daily"
  RTKLIB_processing <- TRUE
  #virtual_sensor <- "matterhorn_gps_differential__batch__daily"
  virtual_sensor_inclino <- "matterhorn_gps_inclinometer"
  try(source('../r_toolchain/permasense_gps_exporter.R'))
}


#positions <- list(42, 33, 34, 35, 40)
positions <- list(42, 33, 34, 35)

for (position in positions) {
  mh_dat <- xts()

  if (position == 42){
    mh_dat <- gps_data_HOGR
  } else {
    mh_dat <- get("gps_data_MH"&position)
  }
  
  correction_min_date = min(mh_dat$generation_time)
  correction_max_date = max(mh_dat$generation_time)
  correction_min_index = match(correction_min_date, mh_dat$generation_time)
  correction_max_index = match(correction_max_date, mh_dat$generation_time)
  correction_index = match(correction_max_date, mh_dat$generation_time)
  

  # calculate the offset
  horiz_offset = 1
  mh_dat_e_offset = mean(mh_dat$e[c(horiz_offset)])
  #mh_dat$e[correction_max_index]
  mh_dat_n_offset = mean(mh_dat$n[c(horiz_offset)])
  mh_dat_h_offset = mean(mh_dat$h[c(horiz_offset)])
  
  while (correction_index <= correction_min_index){
    mh_dat$e[correction_index] <- mh_dat$e[correction_index] - mh_dat_e_offset
    mh_dat$n[correction_index] <- mh_dat$n[correction_index] - mh_dat_n_offset
    mh_dat$h[correction_index] <- mh_dat$h[correction_index] - mh_dat_h_offset
    
    correction_index <- correction_index + 1
  }
  
  if (position == 42){
    gps_relative_data_HOGR <- mh_dat
  } else {
    assign("gps_relative_data_MH"&position, mh_dat)
  }

}

windows(width=21/2.54, height=29.1/2.54)
par(mfrow=c(3,1))
plot(gps_relative_data_HOGR$generation_time,gps_relative_data_HOGR$e, type="s", main="Matterhorn Displacement East", ylim=c(-0.35,0.05), ylab="[m]", xlab="")
abline(h=0, lty=3)
lines(gps_relative_data_MH33$generation_time,gps_relative_data_MH33$e, type="s", col="green")
lines(gps_relative_data_MH34$generation_time,gps_relative_data_MH34$e, type="s", col="red")
lines(gps_relative_data_MH35$generation_time,gps_relative_data_MH35$e, type="s", col="blue")
#lines(gps_relative_data_MH40$generation_time,gps_relative_data_MH40$e, type="s", col="grey")
legend("topleft", inset=0.05, legend=c("HOGR "&gps_total_displacements_HOGR$east_displacement*100&" cm, "&gps_total_displacements_HOGR$east_rate*100&" cm/a", 
                                       "MH33 "&gps_total_displacements_MH33$east_displacement*100&" cm, "&gps_total_displacements_MH33$east_rate*100&" cm/a", 
                                       "MH34 "&gps_total_displacements_MH34$east_displacement*100&" cm, "&gps_total_displacements_MH34$east_rate*100&" cm/a", 
                                       "MH35 "&gps_total_displacements_MH35$east_displacement*100&" cm, "&gps_total_displacements_MH35$east_rate*100&" cm/a"), 
#                                       "MH40 "&gps_total_displacements_MH40$east_displacement*100&" cm, "&gps_total_displacements_MH40$east_rate*100&" cm/a"), 
       col=c("black", "green", "red", "blue", "grey"), lty=1:1, cex=1.1, bg="white")

plot(gps_relative_data_HOGR$generation_time,gps_relative_data_HOGR$n, type="s", main="Matterhorn Displacement North", ylim=c(-0.05,0.35), ylab="[m]", xlab="")
abline(h=0, lty=3)
lines(gps_relative_data_MH33$generation_time,gps_relative_data_MH33$n, type="s", col="green")
lines(gps_relative_data_MH34$generation_time,gps_relative_data_MH34$n, type="s", col="red")
lines(gps_relative_data_MH35$generation_time,gps_relative_data_MH35$n, type="s", col="blue")
#lines(gps_relative_data_MH40$generation_time,gps_relative_data_MH40$n, type="s", col="grey")
legend("topleft", inset=0.05, legend=c("HOGR "&gps_total_displacements_HOGR$north_displacement*100&" cm, "&gps_total_displacements_HOGR$north_rate*100&" cm/a", 
                                          "MH33 "&gps_total_displacements_MH33$north_displacement*100&" cm, "&gps_total_displacements_MH33$north_rate*100&" cm/a", 
                                          "MH34 "&gps_total_displacements_MH34$north_displacement*100&" cm, "&gps_total_displacements_MH34$north_rate*100&" cm/a", 
                                          "MH35 "&gps_total_displacements_MH35$north_displacement*100&" cm, "&gps_total_displacements_MH35$north_rate*100&" cm/a"), 
#                                          "MH40 "&gps_total_displacements_MH40$north_displacement*100&" cm, "&gps_total_displacements_MH40$north_rate*100&" cm/a"), 
       col=c("black", "green", "red", "blue", "grey"), lty=1:1, cex=1.1, bg="white")

plot(gps_relative_data_HOGR$generation_time,gps_relative_data_HOGR$h, type="s", main="Matterhorn Displacement Altitude", ylim=c(-0.15,0.25), ylab="[m]", xlab="")
abline(h=0, lty=3)
lines(gps_relative_data_MH33$generation_time,gps_relative_data_MH33$h, type="s", col="green")
lines(gps_relative_data_MH34$generation_time,gps_relative_data_MH34$h, type="s", col="red")
lines(gps_relative_data_MH35$generation_time,gps_relative_data_MH35$h, type="s", col="blue")
#lines(gps_relative_data_MH40$generation_time,gps_relative_data_MH40$h, type="s", col="grey")
legend("bottomleft", inset=0.05, legend=c("HOGR "&gps_total_displacements_HOGR$vertical_displacement*100&" cm, "&gps_total_displacements_HOGR$vertical_rate*100&" cm/a", 
                                          "MH33 "&gps_total_displacements_MH33$vertical_displacement*100&" cm, "&gps_total_displacements_MH33$vertical_rate*100&" cm/a", 
                                          "MH34 "&gps_total_displacements_MH34$vertical_displacement*100&" cm, "&gps_total_displacements_MH34$vertical_rate*100&" cm/a", 
                                          "MH35 "&gps_total_displacements_MH35$vertical_displacement*100&" cm, "&gps_total_displacements_MH35$vertical_rate*100&" cm/a"), 
#                                          "MH40 "&gps_total_displacements_MH40$vertical_displacement*100&" cm, "&gps_total_displacements_MH40$vertical_rate*100&" cm/a"), 
       col=c("black", "green", "red", "blue", "grey"), lty=1:1, cex=1.1, bg="white")

#print plot to pdf
plot_name <- "mh_gps_displacements_alltime.pdf"
dev.copy(pdf,plot_name, width=21/2.54, height=29.1/2.54)
dev.off()



## PLOT ANNUAL DISPLACEMENTS
plot_title <- as.character("Matterhorn Annual Displacements")

par(mfrow=c(5,1))
plot(gps_annual_displacements_HOGR$avg_window_start_date, gps_annual_displacements_HOGR$e_displacement, type="s", ylim=c(-0.05,0.1), xlab="", ylab="[m]", main=plot_title)
abline(h=0, lty=3)
lines(gps_annual_displacements_MH33$avg_window_start_date, gps_annual_displacements_MH33$e_displacement, type="s", col="green")
lines(gps_annual_displacements_MH34$avg_window_start_date, gps_annual_displacements_MH34$e_displacement, type="s", col="red")
lines(gps_annual_displacements_MH35$avg_window_start_date, gps_annual_displacements_MH35$e_displacement, type="s", col="blue")

plot(gps_annual_displacements_HOGR$avg_window_start_date, gps_annual_displacements_HOGR$n_displacement, type="s", ylim=c(-0.1,0.05), xlab="", ylab="[m]", main=plot_title)
abline(h=0, lty=3)
lines(gps_annual_displacements_MH33$avg_window_start_date, gps_annual_displacements_MH33$n_displacement, type="s", col="green")
lines(gps_annual_displacements_MH34$avg_window_start_date, gps_annual_displacements_MH34$n_displacement, type="s", col="red")
lines(gps_annual_displacements_MH35$avg_window_start_date, gps_annual_displacements_MH35$n_displacement, type="s", col="blue")

plot(gps_annual_displacements_HOGR$avg_window_start_date, gps_annual_displacements_HOGR$horiz_displacement, type="s", ylim=c(-0.05,0.1), xlab="", ylab="[m]", main=plot_title)
abline(h=0, lty=3)
lines(gps_annual_displacements_MH33$avg_window_start_date, gps_annual_displacements_MH33$horiz_displacement, type="s", col="green")
lines(gps_annual_displacements_MH34$avg_window_start_date, gps_annual_displacements_MH34$horiz_displacement, type="s", col="red")
lines(gps_annual_displacements_MH35$avg_window_start_date, gps_annual_displacements_MH35$horiz_displacement, type="s", col="blue")

plot(gps_annual_displacements_HOGR$avg_window_start_date, gps_annual_displacements_HOGR$vertical_displacement, type="s", ylim=c(-0.1,0.05), xlab="", ylab="[m]", main=plot_title)
abline(h=0, lty=3)
lines(gps_annual_displacements_MH33$avg_window_start_date, gps_annual_displacements_MH33$vertical_displacement, type="s", col="green")
lines(gps_annual_displacements_MH34$avg_window_start_date, gps_annual_displacements_MH34$vertical_displacement, type="s", col="red")
lines(gps_annual_displacements_MH35$avg_window_start_date, gps_annual_displacements_MH35$vertical_displacement, type="s", col="blue")

plot(gps_annual_displacements_HOGR$avg_window_start_date, gps_annual_displacements_HOGR$total_displacement, type="s", ylim=c(-0.05,0.1), xlab="", ylab="[m]", main=plot_title)
abline(h=0, lty=3)
lines(gps_annual_displacements_MH33$avg_window_start_date, gps_annual_displacements_MH33$total_displacement, type="s", col="green")
lines(gps_annual_displacements_MH34$avg_window_start_date, gps_annual_displacements_MH34$total_displacement, type="s", col="red")
lines(gps_annual_displacements_MH35$avg_window_start_date, gps_annual_displacements_MH35$total_displacement, type="s", col="blue")


#print plot to pdf
#plot_name <- "gps_"&label&"_processing_status.pdf"
#dev.copy(pdf,plot_name, width=21/2.54, height=29.7/2.54)
#dev.off()

