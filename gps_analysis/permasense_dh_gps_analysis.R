## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
##
## PermaSense Mattertal Joint GPS Data Analysis Tool
##
## This allows to analyze PermaSense GNSS kinematic data
## - Plotting=
## - Data Export
##
## @author: Jan Beutel
## @date:   May 13, 2019
## @version v1.0
##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##


##====================================================================
## PREPARATION
require(xts)

setwd("~/permasense_engineering/software/gps/gps_permos_toolchain")
setwd("V:/scratch/permasense_dataexport_tools/gps_analysis")

## construct method to concatenate strings with ampersand
"&" <- function(...) UseMethod("&") 
"&.default" <- .Primitive("&") 
"&.character" <- function(...) paste(...,sep="")

#positions <- list(42, 33, 34, 35, 40)
positions <- list(89, 91, 82,
  #39, 41, 43, 44, 82, #00, 07, 15, 17,
                  #55, 63,
                  #06, 09, 70, 
                  #43, 44, 64, 83, 84, 
                  #15, 05, 17, 86, 87,
                  #00, 05, 06, 07, 09, 12,
                  #15, 17,
                  #21, 23, 25, 27, 29, 31, 33, 35,
                  #39, 41, 43, 44,
                  #55, 56, 
  57, 62, 63, 64, 66, 67, 70,
                  81, 82, 83, 84, 86, 87, 88, 89, 91)

date_beg <- "15/12/2010+00:00:00"
#date_beg <- "08/03/2011+00:00:00"
#date_beg <- "02/05/2011+00:00:00"
#date_beg <- "01/01/2019+00:00:00"
date_beg_orig <- date_beg

#date_end <- "01/06/2011+00:00:00"
date_end <- strftime(Sys.Date(), format="%d/%m/%Y+%H:%M:%S")

for (position in positions) {
  #    assign("gps__data_MH"&position, mh_dat)
  #position = 87
  if (position == 12) {
    date_beg <- "07/10/2015+00:00:00"
  } else if(position == 15) {
    date_beg <- "02/05/2011+00:00:00"
  } else if(position == 17) {
    date_beg <- "02/05/2011+00:00:00"
  } else if(position == 43) {
    date_beg <- "01/10/2011+00:00:00"
  } else if(position == 44) {
    date_beg <- "01/10/2011+00:00:00"
  } else if(position == 63) {
    date_beg <- "01/03/2012+00:00:00"
  }
  else {
    date_beg <- date_beg_orig
  }
  virtual_sensor <- "dirruhorn_gps_differential__rtklib__daily"
  RTKLIB_processing <- TRUE
  #virtual_sensor <- "dirruhorn_gps_differential__batch__daily"
  virtual_sensor_inclino <- "dirruhorn_gps_inclinometer"
  try(source('../r_toolchain/permasense_gps_exporter.R'))
}

positions <- list(01, 02, 03, 04, 05, 06, 07, 08, 09, 10)
#position = 3
for (position in positions) {
  virtual_sensor <- "permos_gps_differential__rtklib__daily"
  virtual_sensor_inclino <- "permos_gps_inclinometer"
  try(source('../r_toolchain/permasense_gps_exporter.R'))
}


# Plot All Landforms
############################################################################################################
par(mfrow=c(4,1), mar=c(2,4,3,1), oma=c(2,1,4,1)) #bottom, left, top, right
plot_title <- as.character(label&" - Position "&position&" - "&as.Date(min(get("gps_data_"&label)$generation_time))&" to "&as.Date(max(get("gps_data_"&label)$generation_time)))

label = "DI07"
plot(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5,
     main="Displacements Fast Rockglaciers Mattertal", ylab="m/month", xlab="", 
     ylim=c(0,1.5))
#label = "DI07"
#lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.4, lty=3)
label = "DI03"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="darkgreen", cex=0.5)
label = "DI04"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.5)
label = "GG02"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="purple", cex=0.5)
label = "BH10"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="red", cex=0.5)
abline(h=0, lty=3, col="grey")
legend("topleft", inset=0.05, legend=c("DI07", "DI03", "DI04", "GG02", "BH10"), col=c("blue", "darkgreen", "orange", "purple", "red"), lty=1:1, cex=1.1, bg="white") 
       

#plot Di07 again for scaling
label = "DI07"
plot(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$avg_v_horiz,type="s", col="white", cex=0.5, 
      main="Displacements Slow Rockglaciers Mattertal", ylab="m/month", xlab="", 
     ylim=c(0,0.4))
label = "DI02"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5)
#label = "DI55"
#lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5)
label = "ST02"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.5)
label = "ST05"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.5)
label = "BH13"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="red", cex=0.5)
label = "GG01"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="purple", cex=0.5)
label = "RIT1"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="darkgreen", cex=0.5)
abline(h=0, lty=3, col="grey")
legend("topleft", inset=0.05, legend=c("DI02", "ST02", "ST05", "BH13", "GG01", "RIT1"), col=c("blue", "orange", "orange", "red", "purple", "darkgreen"), lty=1:1, cex=1.1, bg="white") 


#plot Di07 again for scaling
label = "DI07"
plot(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$avg_v_horiz,type="s", col="white", cex=0.5, 
     main="Displacements Landslides Mattertal", ylab="m/month", xlab="", 
     ylim=c(0,0.3))

label = "WYS1"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="brown", cex=2.5)
label = "LS05"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="purple", cex=0.8)
label = "LS11"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="purple", cex=0.8)
label = "LS12"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="purple", cex=0.8)
label = "BH09"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.8)
label = "BH07"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.8)
label = "BH12"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.8)
label = "BH03"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.8)
abline(h=0, lty=3, col="grey")
legend("topleft", inset=0.05, legend=c("WYS1", "LS05", "LS11", "LS12", "BH09", "BH07", "BH12", "BH03"), col=c("brown", "purple", "purple", "purple", "orange", "orange", "orange", "orange"), lty=1:1, cex=1.1, bg="white") 


label = "RG01"
plot(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5,
     main="Displacements Solid Rock Features Mattertal", ylab="m/month", xlab="", 
     ylim=c(0,0.035))
label = "GG66"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5)
label = "GG67"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="blue", cex=0.5)
label = "LS06"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="red", cex=0.5)
label = "SATT"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="darkgreen", cex=0.5)
label = "SA01"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="darkgreen", cex=0.5)
label = "HOGR"
lines(get("gps_monthly_displacements_"&label)$avg_window_start_date,get("gps_monthly_displacements_"&label)$total_displacement,type="s", col="orange", cex=0.5)
abline(h=0, lty=3, col="grey")
legend("topleft", inset=0.05, legend=c("RG01", "GG66", "GG67", "LS06", "SATT", "SA01", "HOGR"), col=c("blue", "blue", "blue", "red", "darkgreen", "darkgreen", "orange"), lty=1:1, cex=1.1, bg="white") 






# plot(get("gps_data_"&label)$generation_time,get("gps_data_"&label)$n,type="p", col="blue", cex=0.2, main="Displacement North", ylab="North", xlab="")
# legend("topleft", inset=0.05, lty=1, cex=0.6,
#        legend=c("Total north displacement: "&north_displacement&" m\n",
#                 "Average north displacement rate: "&north_rate&" m/a\n"))
# 
# plot(get("gps_data_"&label)$generation_time,get("gps_data_"&label)$h,type="p", col="blue", cex=0.2, main="Displacement Altitude", ylab="Altitude [m]", xlab="")
# legend("bottomleft", inset=0.05, lty=1, cex=0.6,
#        legend=c("Total vertical displacement: "&vertical_displacement&" m\n",
#                 "Average vertical displacement rate: "&vertical_rate&" m/a\n",
#                 "Total displacement: "&total_displacement&" m\n",
#                 "Average total displacement rate: "&total_rate&" m/a\n"))
# 
# mtext(plot_title, outer = TRUE, cex = 1.5)
# 
# dev.copy(pdf,"gps_inclinometer_"&label&".pdf", width=21/2.54, height=29.7/2.54)
# dev.off()


dev.copy(pdf,"dh_gps_displacements.pdf", width=21/2.54, height=29.7/2.54)
dev.off()

 print("Done. Go have an ice cream...")



#print plot to pdf
#plot_name <- "mh_gps_displacements_alltime.pdf"
#dev.copy(pdf,plot_name, width=21/2.54, height=29.1/2.54)
#dev.off()

