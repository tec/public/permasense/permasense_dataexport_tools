## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
##
## PermaSense Geophone Data Export & Analysis Tool
##
## This allows access PermaSense Geophone data
## - Data is imported from GSN DB server, plotted on a 1-page overview
## - A CSV data file is generated
##
## @author: Jan Beutel
## @date:   May 18, 2019
## @version v1.1
##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##

##====================================================================
## PREPARATION

require(zoo)
require(xts)

# set miliseconds time resolution
options(digits.secs = 6)

## construct method to concatenate strings with ampersand 
"&" <- function(...) UseMethod("&")
"&.default" <- .Primitive("&")
"&.character" <- function(...) paste(...,sep="")

##=====================================================================
## DEFINE DIFFERENT VARIABLES (CHANGE TO YOUR NEEDS)

# all input times are UTC!!!
#date_beg <- "07/02/2020+22:00:00"
#date_end <- "24/05/2018+08:00:00"
#date_end <- strftime(Sys.time(), format="%d/%m/%Y+%H:%M:%S", tz="UTC")

date_beg_string <- strftime(as.POSIXct(date_beg, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")
date_end_string <- strftime(as.POSIXct(date_end, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")

## set server
#server <- "http://tpbl.permasense.ethz.ch/"
#virtual_sensor <- "etz_dpp_geophone_acq__mapped"

server <- "http://data.permasense.ch/"
#virtual_sensor <- "dirruhorn_dpp_geophone_acq__mapped"
#virtual_sensor <- "matterhorn_dpp_geophone_acq__mapped"

#ETZ 83, 84, 88, 95, 105, 107, 118, 126
#position=83
checkplots = TRUE

##=====================================================================
## CREATING URL FOR GSN-REQUEST
## check document gsn-datadistribution-one-shot.pdf and data_access.rtf
## url<-"http://data.permasense.ch/multidata?vs[1]=dirruhorn_gps_raw__mapped&field[1]=gps_time,gps_week&from=09/02/2012+12:00:00&to=09/02/2012+13:00:00&timeline=generation_time&c_join[1]=and&c_vs[1]=dirruhorn_gps_raw__mapped&c_field[1]=position&c_min[1]=38&c_max[1]=39"

## selection by time intervall, position ordered by generation_time
query<- "vs[1]=" & virtual_sensor & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor & "&c_field[1]=position&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time"
# by device_id
#query<- "vs[1]=" & virtual_sensor & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor & "&c_field[1]=device_id&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time"

## construct url:
url<-server & "multidata?" & query 
#print("http request is: "&url)

##=====================================================================
## ACCESS TO GSN DATABASE
## accessing data and making of data frame (from csv export)
## skip header lines (first 2) for import
dat <- data.frame()
assign("geophone_data_"&position, dat)

dat <- read.csv(url, skip=2, stringsAsFactors=FALSE)

# fix first column naming
names(dat)[1]<-paste("position")

##=====================================================================

# calculate gap data
dat <- cbind(dat, dat$start_time)
names(dat)[28] <- paste("trg_delay")

dat <- cbind(dat, dat$start_time)
names(dat)[29] <- paste("end_time")

dat <- cbind(dat, dat$start_time)
names(dat)[30] <- paste("trg_duration")

dat <- cbind(dat, dat$samples)
names(dat)[31] <- paste("gap")

dat <- cbind(dat, dat$id)
names(dat)[32] <- paste("id_inc")
dat$id_inc <- as.numeric(dat$id_inc)


i=1

while (i < (length(dat$samples)+1)){
  #trigger delay [msec]
  dat$trg_delay[i] <- (dat$first_time[i] - dat$start_time[i]) / 1000
  
  if (dat$trg_last_pos_sample[i] > dat$trg_last_neg_sample[i]){
    dat$end_time[i] <- (dat$first_time[i] + dat$trg_last_pos_sample[i]*1000)
  } else {
    dat$end_time[i] <- (dat$first_time[i] + dat$trg_last_neg_sample[i]*1000)
  }

  #trigger duration [sec]
  dat$trg_duration[i] = (dat$end_time[i] - dat$start_time[i]) / 1000000
  
  #event gap [sec]
  if (i < (length(dat$samples) - 0) ){
    dat$gap[i] <- (dat$end_time[i] - dat$start_time[i+1]) / 1000000
    dat$id_inc[i] <- (as.numeric(dat$id_inc[i]) - as.numeric(dat$id_inc[i+1]))
  } else {
    dat$gap[i] = 1
    dat$id_inc[i] = 1
  }
  #correct adc_pga = zero
  if (dat$adc_pga[i] == 0) {
    dat$adc_pga[i] = 1
  }
  #/* ADC rate (sampling frequency), 0 = 1kHz, 1 = 500Hz, 2 = 250Hz, 3 = 125Hz */
  if (dat$adc_sps[i] == 0) {
    dat$adc_sps[i] = 1000
  } else if (dat$adc_sps[i] == 1) {
    dat$adc_sps[i] = 500
  } else if (dat$adc_sps[i] == 2) {
    dat$adc_sps[i] = 250
  } else if (dat$adc_sps[i] == 3) {
    dat$adc_sps[i] = 125
  }
  i = i+1
}

#remove null values (sampling/gsn mapping errors)
null_list <- match("null", dat$adc_pga)
#print(null_list)
for(nullposition in null_list) {
  dat$adc_pga[nullposition] = 1
}

#scale peak values - 2^23 is 8388608, 16.7 Mio levels, miliVolts
#dat$peak_pos_val <- ((as.numeric(dat$peak_pos_val, na.rm=TRUE) - 2^23)) * 2500 / (as.numeric(dat$adc_pga) * (2^24-2))
#dat$peak_neg_val <- ((as.numeric(dat$peak_neg_val, na.rm=TRUE) - 2^23)) * 2500 / (as.numeric(dat$adc_pga) * (2^24))

dat$peak_pos_val <- ((as.numeric(dat$peak_pos_val, na.rm=TRUE) - 2^23)) * 2500 / (2^24-2) / (as.numeric(dat$adc_pga))
dat$peak_neg_val <- ((as.numeric(dat$peak_neg_val, na.rm=TRUE) - 2^23)) * 2500 / (2^24)   / (as.numeric(dat$adc_pga))
# SM-6 Omni 3500 Ohm 80 V/m/s

## Convert timestamps to UTC
dat$timestamp <- as.POSIXct(dat$timestamp/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$generation_time <- as.POSIXct(dat$generation_time/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$generation_time_microsec <- as.POSIXct(dat$generation_time_microsec/1000000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$start_time <- as.POSIXct(dat$start_time/1000000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$first_time <- as.POSIXct(dat$first_time/1000000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$end_time <- as.POSIXct(dat$end_time/1000000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")

# reorder some columns
refcols <- c("position", "timestamp", "seqnr", "id_inc",
             "start_time", "trg_delay", "first_time", "trg_duration", "end_time", "generation_time_microsec", "gap", 
             "trg_source", "trg_count_pos", "trg_count_neg", "trg_last_pos_sample", "trg_last_neg_sample", 
             "peak_pos_val", "peak_pos_sample", "peak_neg_val", "peak_neg_sample", "samples", 
             "trg_gain", "trg_th_pos", "trg_th_neg", "adc_pga", "target_id", "device_id", "message_type", "payload_length")
dat <- dat[, c(refcols, setdiff(names(dat), refcols))]
names(dat)

#print(max(dat$payload_length))

# Plot Trigger Event Characteristics
############################################################################################################
if (checkplots == TRUE) {
  par(mfrow=c(10,1), mar=c(1,4,2,1), oma=c(2,1,1,1))
  xlimstring = c(as.POSIXct(date_beg, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"),
                 as.POSIXct(date_end, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"))
  ylimstring <- c(0,180)
  colors <- ifelse(dat$trg_source >= 2, "red", "blue")
  
  plot(as.POSIXlt(dat$start_time),dat$trg_duration, type="p", xlim = xlimstring, ylim = ylimstring,
       main="Trigger Events - Position "&position&" - "&date_beg_string&" to "&date_end_string, xlab="", ylab="Duration [sec]", 
       col=colors)
  abline(h=mean(dat$trg_duration), lty=3)
  legend("top", inset=0.05, cex=1.0, bg="white",
         legend=c("Number of events: "&length(dat$samples),
                  "Maximum event duration: "&round(max(dat$trg_duration), digits=3)&" sec, Minimum event duration: "&round(min(dat$trg_duration), digits=3)&" sec, Average event duration: "&round(mean(dat$trg_duration), digits=3)&" sec",
                  "Trigger Thresholds/Gain: "&round(mean(dat$trg_th_pos), digits=1)&"/"&round(mean(dat$trg_th_neg), digits=1)&"/"&round(mean(dat$trg_gain), digits=1)
         ))
  
  hist(dat$trg_duration, freq=TRUE, breaks=1000, xlab="", main="")
  
  plot(as.POSIXlt(dat$start_time),dat$gap, type="p", xlab="", ylab="Event Gap [sec]", col=colors, xlim = xlimstring, main="", xaxt='n')
  abline(h=mean(dat$gap), lty=3)
  legend("topleft", inset=0.05, cex=1.0, bg="white",
         legend=c("Maximum event gap: "&round(max(dat$gap, na.rm=TRUE), digits=3)&" sec",
                  "Minimum event gap: "&round(min(dat$gap, na.rm=TRUE), digits=3)&" sec",
                  "Average event gap: "&round(mean(dat$gap, na.rm=TRUE), digits=3)&" sec"
         ))
  
  hist(dat$gap, breaks=5000, freq=TRUE, xlab="", main="")
  
  plot(as.POSIXlt(dat$start_time),1/abs(dat$gap), xlab="", ylab="Inverse Gap [1/sec]", col=colors, xlim = xlimstring, main="", xaxt='n')
  
  plot(as.POSIXlt(dat$start_time),dat$id_inc, pch=20, ylim=c(0,(max(dat$id_inc, na.rm=TRUE)+1)),  xlim = xlimstring, main="", xlab="", ylab="Acq. Seq. No. Inc.", col=colors, xaxt='n')
  
  plot(as.POSIXlt(dat$start_time),dat$trg_delay, pch=20, main="", xlab="", ylab="Trigger Delay [msec]", xlim = xlimstring, col=colors, xaxt='n')
  
  ylimstring <- c(-max(dat$trg_count_neg),max(dat$trg_count_pos))
  plot(as.POSIXlt(dat$start_time),dat$trg_count_pos, pch=20, xlab="", ylab="Trigger Count", ylim=ylimstring, xlim = xlimstring, col="blue", xaxt='n')
  lines(as.POSIXlt(dat$start_time),-dat$trg_count_neg, type="p", pch=20, xlab="", ylab="Trigger Count Neg", xlim = xlimstring, col="red", xaxt='n')
  abline(h=0, lty=3)
  
  ylimstring <- c(-10,10)
  ylimstring <- c(-5,5)
  ylimstring <- c(floor(min(dat$peak_neg_val, na.rm=TRUE))*1.1,ceiling(max(dat$peak_pos_val, na.rm=TRUE))*1.1)
  plot(as.POSIXlt(dat$start_time),dat$peak_pos_val,  pch=20, xlab="", ylab="Peak Values [mV]", xlim = xlimstring, ylim=ylimstring, col="blue", xaxt='n')
  lines(as.POSIXlt(dat$start_time),dat$peak_neg_val, type="p", pch=20, xlab="", ylab="", xlim = xlimstring, col="red", xaxt='n')
  abline(h=0, lty=3)
  
  ylimstring <- c(-max(dat$peak_neg_sample),max(dat$peak_pos_sample))
  plot(as.POSIXlt(dat$start_time),dat$peak_pos_sample, pch=20, xlab="", ylab="Pos Rise Time [msec]", ylim=ylimstring, xlim = xlimstring, col="blue", xaxt='n')
  lines(as.POSIXlt(dat$start_time),-(as.numeric(dat$peak_neg_sample)), type="p", pch=20, xlab="", ylab="", xlim = xlimstring, col="red")
  abline(h=0, lty=3)
  
  dev.copy(pdf,"trigger_events"&position&".pdf", width=21/2.54, height=29.7/2.54)
  dev.off()
}

##=====================================================================
## Export data

# export to R variables
assign("geophone_data_"&position, dat)

# Export to CSV file
#file_name <- "gps_data_"&label&".csv"
#file_header <- "Differential GPS Position Data File (c) ETH Zurich 2017 \n"
#file_subheader <- plot_title&"\n\n"&

#cat(file_header, '\n', file = file_name)
#cat(file_subheader,  '\n', file = file_name, append = TRUE)
#suppressWarnings(write.table(annual_displacements, file = file_name, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE))
#cat("\nDaily positions are 12:00:00 UTC centered using the generation_time column\nGeodetic datum is CH1903+/LV95\nReference Frame Bessel (ellipsoidal)\n", '\n', file = file_name, append = TRUE)
#suppressWarnings(write.table(dat, file = file_name, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE))

#write.table_with_header <- function(x, file, header, ...){
#  cat(header, '\n',  file = file)
#  write.table(x, file, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE)
#}

print("Position "&position&" done. Geophone data ready.")

