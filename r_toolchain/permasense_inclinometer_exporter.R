## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
##
## PermaSense Inclinometer Data Export & Anlysis Tool
##
## This allows access PermaSense Inclinometer data
## - Data is imported from GSN DB server, plotted on a 1-page overview
## - A CSV data file is generated
##
## @author: Jan Beutel
## @date:   March 28, 2019
## @version v1.1
##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##

##====================================================================
## PREPARATION

require(zoo)
require(xts)

# set miliseconds time resolution
options(digits.secs = 6)

## construct method to concatenate strings with ampersand 
"&" <- function(...) UseMethod("&")
"&.default" <- .Primitive("&")
"&.character" <- function(...) paste(...,sep="")

##=====================================================================
## DEFINE DIFFERENT VARIABLES (CHANGE TO YOUR NEEDS)

# all input times are UTC!!!
#date_beg <- "15/12/2010+00:00:00"
#date_beg <- "17/12/2015+00:00:00"
#date_end <- "22/07/2018+14:00:00"
#date_end <- strftime(Sys.time(), format="%d/%m/%Y+%H:%M:%S", tz="UTC")

date_beg_string <- strftime(as.POSIXct(date_beg, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")
date_end_string <- strftime(as.POSIXct(date_end, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")

## set server
#server <- "http://data.permasense.ch/"
#virtual_sensor_inclino <- "dirruhorn_gps_inclinometer"
#virtual_sensor_inclino <- "matterhorn_gps_inclinometer"
#virtual_sensor_inclino <- "permos_gps_inclinometer"

#position=87

##=====================================================================
## CREATING URL FOR GSN-REQUEST
## check document gsn-datadistribution-one-shot.pdf and data_access.rtf
## url<-"http://data.permasense.ch/multidata?vs[1]=dirruhorn_gps_raw__mapped&field[1]=gps_time,gps_week&from=09/02/2012+12:00:00&to=09/02/2012+13:00:00&timeline=generation_time&c_join[1]=and&c_vs[1]=dirruhorn_gps_raw__mapped&c_field[1]=position&c_min[1]=38&c_max[1]=39"

## selection by time intervall, position ordered by generation_time
query<- "vs[1]=" & virtual_sensor_inclino & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor_inclino & "&c_field[1]=position&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time"
# by device_id
#query<- "vs[1]=" & virtual_sensor_inclino & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor_inclino & "&c_field[1]=device_id&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time"

# with interval
#&agg_function=avg&agg_unit=3600000&agg_period="
query<- "vs[1]=" & virtual_sensor_inclino & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor_inclino & "&c_field[1]=position&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time&agg_function=avg&agg_unit=300000&agg_period=12"


## construct url:
url<-server & "multidata?" & query 
#print("http request is: "&url)

##=====================================================================
## ACCESS TO GSN DATABASE
## accessing data and making of data frame (from csv export)
## skip header lines (first 2) for import
dat <- data.frame()
dat <- read.csv(url, skip=2, stringsAsFactors=FALSE)

# fix first column naming
names(dat)[1]<-paste("position")
#extract label from GPS exporter tool
#label <- as.character(dat$label[1])

## Convert timestamps to UTC
dat$timestamp <- as.POSIXct(dat$timestamp/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$gsn_timestamp <- as.POSIXct(dat$gsn_timestamp/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")
dat$generation_time <- as.POSIXct(dat$generation_time/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")

# reorder some columns
#refcols <- c("position", "timestamp", "seqnr", "id_inc",
#             "start_time", "trg_delay", "first_time", "trg_duration", "end_time", "generation_time_microsec", "gap",
#             "trg_source", "trg_count_pos", "trg_count_neg", "trg_last_pos_sample", "trg_last_neg_sample",
#             "peak_pos_val", "peak_pos_sample", "peak_neg_val", "peak_neg_sample", "samples",
#             "trg_gain", "trg_th_pos", "trg_th_neg", "adc_pga", "target_id", "device_id", "message_type", "payload_length")
#dat <- dat[, c(refcols, setdiff(names(dat), refcols))]
#names(dat)

downsampling_factor = 60*24
downsampling_factor = 60*6

dat.xts <- xts(dat$inclinometer_x, dat$generation_time)
ep<-endpoints(dat.xts, on="minutes", k=downsampling_factor)  #on="how your data is spaced",k=how you want to club it
dat.xts.downsampled<-period.apply(dat.xts, FUN=mean,INDEX=ep)

dat2.xts <- xts(dat$inclinometer_y, dat$generation_time)
ep<-endpoints(dat2.xts, on="minutes", k=downsampling_factor)  #on="how your data is spaced",k=how you want to club it
dat2.xts.downsampled<-period.apply(dat2.xts, FUN=mean,INDEX=ep)
dat.downsampled <- data.frame()
dat.downsampled <- cbind(fortify.zoo(dat.xts.downsampled, names=c("generation_time")), fortify.zoo(dat2.xts.downsampled)[,2])
names(dat.downsampled)[2] <- paste("inclinometer_x")
names(dat.downsampled)[3] <- paste("inclinometer_y")

# Detect device changes
############################################################################################################
i=1
inclinometer_device_changes <- data.frame()

while (i < length(dat$device_id)){
  if (dat$device_id[i] != dat$device_id[i+1]){
    inclinometer_device_changes <- rbind(inclinometer_device_changes, data.frame(dat$generation_time[i+1], dat$generation_time[i]))
    #dat$generation_time[i]
  }
  i = i+1
}
inclinometer_device_changes <- rbind(inclinometer_device_changes, data.frame(dat$generation_time[i+1], dat$generation_time[i]))

# Plot Inclinometer Data
############################################################################################################
# par(mfrow=c(2,1), mar=c(2,4,3,1), oma=c(2,1,4,1)) #bottom, left, top, right
# plot_title <- as.character(label&" - Position "&position&" - "&as.Date(min(dat$generation_time))&" to "&as.Date(max(dat$generation_time)))
# 
# plot(dat.downsampled$generation_time, dat.downsampled$inclinometer_x, 
#      main="Inclinometer X", 
#      type="p", pch=20, col="blue", ylab="[deg]")
# abline(h=0, lty=3)
# i=dim(inclinometer_device_changes)[1]
# while (i > 0){
#   abline(v=inclinometer_device_changes[i,1], lty=3, col="grey")
#   abline(v=inclinometer_device_changes[i,2], lty=3, col="grey")
#   i = i -1
# }
# 
# mtext(plot_title, outer = TRUE, cex = 1.5)
# 
# plot(dat.downsampled$generation_time, dat.downsampled$inclinometer_y, 
#      main="Inclinometer Y",
#      type="p", pch=20, col="blue", ylab="[deg]")
# abline(h=0, lty=3)
# i=dim(inclinometer_device_changes)[1]
# while (i > 0){
#   abline(v=inclinometer_device_changes[i,1], lty=3, col="grey")
#   abline(v=inclinometer_device_changes[i,2], lty=3, col="grey")
#   i = i -1
# }

#dev.copy(pdf,"gps_"&label&"_inclinometer.pdf", width=21/2.54, height=29.7/2.54)
#dev.off()


##=====================================================================
## Export data

# export to R variables
assign("inclinometer_data_"&label, dat)
assign("inclinometer_downsampled_data_"&label, dat.downsampled)

# Export to CSV file
file_name <- "gps_"&label&"_inclinometer_data.csv"
file_header <- "Inclinometer Data File (c) ETH Zurich 2019 \n"
file_subheader <- plot_title&"\n"&

cat(file_header, '\n', file = file_name)
cat(file_subheader,  '\n', file = file_name, append = TRUE)
suppressWarnings(write.table(dat.downsampled, file = file_name, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE))

print("Position "&position&" done. Inclinometer data ready.")

