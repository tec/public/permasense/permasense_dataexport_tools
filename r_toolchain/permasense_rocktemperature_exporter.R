## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
##
## PermaSense Groundsurface Temperature Data Export & Anlysis Tool
##
## This allows access PermaSense ADM Borehole data
## - Data is imported from GSN DB server, plotted on a 1-page overview
## - A CSV data file is generated
##
## @author: Jan Beutel
## @date:   April 18, 2019
## @version v1.0
##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##

##====================================================================
## PREPARATION

require(zoo)
require(xts)
library(ggplot2)

setwd("~/permasense_engineering/software/gps/gps_permos_toolchain")
setwd("V:/scratch/permasense_dataexport_tools/r_toolchain")

# set miliseconds time resolution
options(digits.secs = 6)

## construct method to concatenate strings with ampersand 
"&" <- function(...) UseMethod("&")
"&.default" <- .Primitive("&")
"&.character" <- function(...) paste(...,sep="")

##=====================================================================
## DEFINE DIFFERENT VARIABLES (CHANGE TO YOUR NEEDS)

# all input times are UTC!!!
#Jungfraujoch data starts 06/02/2009
date_beg <- "06/02/2009+23:00:00"
#date_beg <- "31/08/2019+23:00:00"
date_end <- strftime(Sys.time(), format="%d/%m/%Y+%H:%M:%S", tz="UTC")

date_beg_string <- strftime(as.POSIXct(date_beg, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")
date_end_string <- strftime(as.POSIXct(date_end, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"), format="%d/%m/%Y+%H:%M:%S %Z")

## set server
server <- "http://data.permasense.ch/"
virtual_sensor <- "adm_temperature_rock"
#virtual_sensor <- "jungfraujoch_temperature_rock"
#virtual_sensor <- "jungfraujoch_temperature_fracture"
#virtual_sensor <- "matterhorn_temperature_rock"
#virtual_sensor <- "matterhorn_temperature_fracture"

#Aig. du Midi 01, 04, 06, 07, 08, 09
#Jungfraujoch PermaSense Rock 01, 02, 03, 04, 05, 06, 07, 09, 22, 25
#Jungfraujoch PermaSense Fracture (Thermistorchain at rock ice boundary) 08, 10
#Jungfraujoch GST (PERMOS) 13, 14, 18
position=08

##=====================================================================
## CREATING URL FOR GSN-REQUEST

## selection by time intervall, position ordered by generation_time
query<- "vs[1]=" & virtual_sensor & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & 
  "&c_join[1]=and&c_vs[1]=" & virtual_sensor & 
  "&c_field[1]=position&c_min[1]=" & position-1 & "&c_max[1]=" & position & 
  "&timeline=generation_time&agg_function=avg&agg_unit=1800000&agg_period=1"  ### agg_unit in msec, agg_period, # of agg_units to export
  "&timeline=generation_time"

# by device_id
#query<- "vs[1]=" & virtual_sensor & "&time_format=unix&field[1]=All&from=" & date_beg & "&to=" & date_end & "&c_join[1]=and&c_vs[1]=" & virtual_sensor & "&c_field[1]=device_id&c_min[1]=" & position-1 & "&c_max[1]=" & position & "&timeline=generation_time"


## construct url:
url<-server & "multidata?" & query 
print("http request is: "&url)

##=====================================================================
## ACCESS TO GSN DATABASE
## accessing data and making of data frame (from csv export)
## skip header lines (first 2) for import
dat <- data.frame()
dat <- read.csv(url, skip=2, stringsAsFactors=FALSE)

# fix first column naming
names(dat)[1]<-paste("position")

##=====================================================================

# check gentime spacing; prepare data frame
dat <- cbind(dat$generation_time, dat)
names(dat)[1] <- paste("gentime_inc")

i=length(dat$generation_time)
dat$gentime_inc[i] = 0

#build gentime increments
i = i-1
while (i > 0){
  dat$gentime_inc[i] <- (dat$generation_time[i] - dat$generation_time[i+1]) / 1000
  
  i = i-1
}

## Convert timestamps to UTC
dat$generation_time <- as.POSIXct(dat$generation_time/1000, origin="1970-01-01", format="%Y-%m-%d", tz="CET")

# reorder some columns
# refcols <- c("position", "timestamp", "seqnr", "id_inc",
#              "start_time", "trg_delay", "first_time", "trg_duration", "end_time", "generation_time_microsec", "gap", 
#              "trg_source", "trg_count_pos", "trg_count_neg", "trg_last_pos_sample", "trg_last_neg_sample", 
#              "peak_pos_val", "peak_pos_sample", "peak_neg_val", "peak_neg_sample", "samples", 
#              "trg_gain", "trg_th_pos", "trg_th_neg", "adc_pga", "target_id", "device_id", "message_type", "payload_length")
# dat <- dat[, c(refcols, setdiff(names(dat), refcols))]
# names(dat)

dat.xts <- xts(dat, order.by=dat$generation_time, na.approx())
dat.xts$gentime_inc <- NULL
dat.xts$generation_time <- NULL
ep<-endpoints(dat.xts, on="hours", k=24)  #on="how your data is spaced",k=how you want to club it
dat.xts.downsampled<-period.apply(dat.xts, FUN=mean, INDEX=ep, na.rm=TRUE)
dat.downsampled <- data.frame()
dat.downsampled <- fortify.zoo(dat.xts.downsampled, names=c("generation_time"))


# Plot Data
############################################################################################################
par(mfrow=c(1,1), mar=c(1,4,2,1), oma=c(3,1,2,1))
xlimstring = c(as.POSIXct(date_beg, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"),
               as.POSIXct(date_end, origin="1970-01-01", format="%d/%m/%Y+%H:%M:%S", tz="UTC"))
ylimstring <- c(-30,30)

# some test
# plot(as.POSIXlt(dat$generation_time),dat$gentime_inc, pch=20, xlab="", ylab="[sec]", col="blue", xlim = xlimstring,
#      main="Generation Time intervals between consecutive samples (should be 120 sec) - Position "&position&" - "&date_beg_string&" to "&date_end_string, xaxt='n')
# abline(h=120, lty=3)
# abline(h=mean(dat$gentime_inc), lty=3)

plot(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_nearsurface_t1, xlim = xlimstring, type="s", pch=20, ylim = ylimstring,
     main="Rock Temperature Data - Position "&position&" - "&date_beg_string&" to "&date_end_string, xlab="", ylab="[C]", col= terrain.colors(12)[1])
abline(h=0, lty=3)
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_nearsurface_t1, type="s", pch=20, col = terrain.colors(12)[2])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_nearsurface_t2, type="s", pch=20, col = terrain.colors(12)[3])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_5cm,   type="s", pch=20, col = terrain.colors(12)[4])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_10cm,  type="s", pch=20, col = terrain.colors(12)[5])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_20cm,  type="s", pch=20, col = terrain.colors(12)[6])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_30cm,  type="s", pch=20, col = terrain.colors(12)[7])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_35cm,  type="s", pch=20, col = terrain.colors(12)[8])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_50cm,  type="s", pch=20, col = terrain.colors(12)[9])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_60cm,  type="s", pch=20, col = terrain.colors(12)[10])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_85cm,  type="s", pch=20, col = terrain.colors(12)[11])
lines(as.POSIXlt(dat.downsampled$generation_time),dat.downsampled$temperature_100cm, type="s", pch=20, col = terrain.colors(12)[12])
legend("top", inset=0.05, cex=1.0, bg="white",
       legend=c("Number of samples: "&length(dat.downsampled$generation_time)
       ))

#print plot to pdf
plot_name <- "temperature_plot_"&position&".pdf"
dev.copy(pdf,plot_name, width=29.7/2.54, height=21/2.54)
dev.off()



# Plot Contourplot - works only for Matterhorn
############################################################################################################
# library("viridis")
# par(mfrow=c(1,1), mar=c(1,4,2,1), oma=c(3,1,2,1))
# 
# x<-c(as.POSIXct(dat.downsampled$generation_time, origin="1970-01-01", format="%d/%m/%Y", tz="UTC"))
# 
# if (position == 7) {
#   depth<-c(-3.0,-2.0,-1.0,-0.1)
#   tempdat <- cbind(
#     dat.downsampled$temperature_300cm,
#     dat.downsampled$temperature_200cm,
#     dat.downsampled$temperature_100cm,
#     dat.downsampled$temperature_10cm)
# }
# 
# 
# if (position == 10) {
#   depth<-c(-0.85,-0.6,-0.35,-0.1,-0.02)
#   tempdat <- cbind(#dat.downsampled$temperature_100cm,
#                    dat.downsampled$temperature_85cm,
#                    dat.downsampled$temperature_60cm,
#                    #dat.downsampled$temperature_50cm,
#                    dat.downsampled$temperature_35cm,
#                    #dat.downsampled$temperature_30cm,
#                    #dat.downsampled$temperature_20cm,
#                    dat.downsampled$temperature_10cm,
#                    #dat.downsampled$temperature_5cm,
#                   dat.downsampled$temperature_nearsurface_t2)
# }
# 
# if (position > 12) {
#   depth<-c(-1,-0.5,-0.3,-0.2,-0.1,-0.05)
#   tempdat <- cbind(dat.downsampled$temperature_100cm,
#                    dat.downsampled$temperature_50cm,
#                    dat.downsampled$temperature_30cm,
#                    dat.downsampled$temperature_20cm,
#                    dat.downsampled$temperature_10cm,
#                    dat.downsampled$temperature_5cm)
# }
# 
# 
# colfunc <- colorRampPalette(c("midnightblue", "blue", "aliceblue", "yellow", "darkred"))
# colfunc(20)
# 
# zlim=c(-20,20)
# nlevels = 40
# 
# filled.contour(x, y=depth, z=tempdat, 
#                zlim=zlim,
#                levels = pretty(zlim, nlevels), nlevels = nlevels,
#                col = colfunc(nlevels),
#                ylab="Depth", xlab="Time", xaxs="i",
#                key.title=title(expression('  Temp ('*degree*'C)'))
# )
# 
# title(main="Rock Temperature Profile - Position "&position)
# 
# dev.copy(pdf,"temperature_contour_"&position&".pdf", width=29.7/2.54, height=21/2.54)
# dev.off()



##=====================================================================
## Export data

# export to R variables
assign("temperature_data_"&position, dat)

# Export to CSV file
file_name <- "temperature_data_position_"&position&".csv"
file_header <- "Rock Temperature Data File (c) ETH Zurich 2019 \n"

cat(file_header, '\n', file = file_name)
#cat("\nDaily positions are 12:00:00 UTC centered using the generation_time column\nGeodetic datum is CH1903+/LV95\nReference Frame Bessel (ellipsoidal)\n", '\n', file = file_name, append = TRUE)
suppressWarnings(write.table(dat, file = file_name, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE))


# export again for downsampled data
assign("temperature_data_downsampled_"&position, dat.downsampled)

# Export to CSV file
file_name <- "temperature_data_downsampled_position_"&position&".csv"
file_header <- "Rock Temperature Data File (c) ETH Zurich 2019 \n"

cat(file_header, '\n', file = file_name)
#cat("\nDaily positions are 12:00:00 UTC centered using the generation_time column\nGeodetic datum is CH1903+/LV95\nReference Frame Bessel (ellipsoidal)\n", '\n', file = file_name, append = TRUE)
suppressWarnings(write.table(dat.downsampled, file = file_name, append = TRUE, quote = FALSE, sep=',', row.names = FALSE, col.names = TRUE))

print("Position "&position&" done. Go have an ice cream...")

